const { expect } = require('chai');
const { vtu } = require('../app/controllers/billsCtrl');


describe('Vtu billing function:', () => {
  it('should return an object on success', () => {
    vtu({
      amount: 100, telco: 'MTN', mobile: '08141804018', eyowo_user: '',
    }).then((data) => {
      expect(data).to.be.a('object');
    }).catch((err) => {
      expect(err).to.be.an('object');
    });
  });
  // it('should return an object on failure', () => {
  //   vtu({
  //     amount: 100, telco: 'MTN', mobile: '08141804018', eyowo_user: '',
  //   }).catch((err) => {
  //     expect(err).to.be.an('object').keys('success', 'message', 'data');
  //   });
  // });
});

// describe('Dstv billing function:', () => {
//   it('should return an object on success', () => {
//     dstv({ amount: 50, decoder: 'happy', eyowo_user: '' })
//       .then((data) => {
//         expect(data).to.be.an('object').keys('success', 'message', 'data');
//       });
//   });
//   it('should return an object on failure', () => {
//     dstv({ amount: 50, decoder: 'happy', eyowo_user: '' })
//       .catch((err) => {
//         expect(err).to.be.an('object').keys('success', 'message', 'data');
//       });
//   });
// });
