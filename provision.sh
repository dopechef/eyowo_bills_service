wget http://nodejs.org/dist/v8.4.0/node-v8.4.0-linux-x64.tar.gz
tar -C /usr/local --strip-components 1 -xzf node-v8.4.0-linux-x64.tar.gz
#clean Up
rm node-v8.4.0-linux-x64.tar.gz

#install git
apt-get install git



#install mongodb
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
#ubuntu 16.04
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo service mongod start


#install nginx
apt-get -y install nginx