const soap = require('soap');
const mongoose = require('mongoose');
const Rabbit = require('../../util/rabbit');
const grpc = require('grpc');

const PROTO_PATH = `${__dirname}/wallet.proto`;
const eyowoMain = grpc.load(PROTO_PATH).eyowo_main;

const switchITUrl = 'https://www.etranzact.net/SwitchIT/doc.wsdl';
const cli = new eyowoMain.Main('35.193.171.6:50052', grpc.credentials.createInsecure());

// grpc client to record transactions
const addTransaction = (typ, userid, amt) => {
  Rabbit.publish('amqps://eyowo:eyowo@portal1464-27.eyowo.3574791653.composedb.com:28897/eyowo', 'transaction-add', JSON.stringify({
    type: typ, amount: amt, user: userid, date: new Date(),
  }));
};

// grpc client to process credits
const creditM = (phone, amt) => new Promise((resolve, reject) => {
  cli.credit({ user: phone, amount: amt }, (err, res) => {
    if (err) throw err;
    resolve(res);
  });
});

// grpc client to process debits
const debitM = (phone, amt) => new Promise((resolve, reject) => {
  cli.debit({ user: phone, amount: amt }, (err, res) => {
    if (err) throw err;
    resolve(res);
  });
});

// to run through the process of vtu payment
const processVTU = async (telco, amount, mobile) => {
  const switchITRequestArgs = {
    request: {
      action: 'VT',
      direction: 'request',
      terminalId: '7017010007',
      transaction: {
        pin: 'GVIXZ19oLpzUbRMHjaQ5Hw==',
        lineType: 'VTU',
        senderName: 'Eyowo',
        address: '91A Ogudu road, Ogudu, Lagos, Nigeria',
        reference: mongoose.Types.ObjectId().toString(),
      },
    },
  };

  if (telco !== 'GLO' && telco !== 'MTN' && telco !== 'ETISALAT' && telco !== 'AIRTEL') {
    return { success: false, error: 'Invalid telco param' };
  }

  // Attempt to deduct from user account
  try {
    const answer = await debitM(mobile, parseInt(amount));
    console.log(answer);
    if (answer.success) {
      switchITRequestArgs.request.transaction.amount = amount.toString();
      switchITRequestArgs.request.transaction.provider = telco;
      switchITRequestArgs.request.transaction.destination = mobile;
      switchITRequestArgs.request.transaction.reference = mongoose.Types.ObjectId().toString();
    } else {
      return { success: false, message: 'Insufficient funds', data: mobile };
    }
  } catch (error) {
    console.log(error);
    return { success: false, data: error };
  }
  // Go ahead to process transfer
  return new Promise((resolve, reject) => {
    soap.createClient(switchITUrl, async (errr, client) => {
      if (errr) {
        // EYOWO MAIN refund user
        const aa = await creditM(mobile, parseInt(amount));
        console.log(aa);
        console.log(errr);
        return reject(errr);
      }

      client.process(switchITRequestArgs, async (errrrr, result) => {
        if (errrrr) {
          console.log(errrrr);
          // Eyowo main refund user
          const aa = await creditM(mobile, parseInt(amount));
          console.log(aa);
          return reject(errrrr);
        }
        if (result.response.error !== '0') {
          const aa = await creditM(mobile, parseInt(amount));
          console.log(aa);

          return resolve({ success: false, message: 'Transaction Failed', data: result });
        }
        // Transaction went through fine
        const bb = await debitM(mobile, parseInt(amount));
        console.log(result);

        // EYOWO MAIN add to transaction history
        const successData = {
          response: {
            reference: result.response.reference,
            message: 'Loaded',
          },
        };

        return resolve({ success: true, data: successData });
      });
    });
  });
};

// to run through the process of dstv payment
const processDSTV = async (mobile, amount, decoderNumber) => {
  const switchITRequestArgs = {
    request: {
      action: 'PB',
      direction: 'request',
      terminalId: '7017010007',
      transaction: {
        pin: 'GVIXZ19oLpzUbRMHjaQ5Hw==',
        lineType: 'DSTV',
        id: '2',
        senderName: 'Eyowo',
        address: '91A Ogudu road, Ogudu, Lagos, Nigeria',
        reference: mongoose.Types.ObjectId().toString(),
      },
    },
  };

  try {
    const answer = await debitM(mobile, parseInt(amount));
    console.log(answer);
    if (answer.success) {
      switchITRequestArgs.request.transaction.amount = amount.toString();
      switchITRequestArgs.request.transaction.destination = decoderNumber;
      switchITRequestArgs.request.transaction.reference = mongoose.Types.ObjectId().toString();
    } else {
      return { success: false, message: 'Insufficient funds', data: decoderNumber };
    }
  } catch (err) {
    console.error(err);
    return { success: false, message: 'Cannot Process Transaction', data: err };
  }
  // Go ahead to process transfer
  return new Promise((resolve, reject) => {
    soap.createClient(switchITUrl, async (errr, client) => {
      if (errr) {
        // refund user
        console.log(errr);
        const answer = await creditM(mobile, parseInt(amount));
        console.log(answer);
        return reject(errr);
      }
      client.process(switchITRequestArgs, async (errrrr, result) => {
        if (errrrr) {
          console.log(errrrr);
          // We couldn't process funds through eTranzact, return the user's funds
          const answer = await creditM(mobile, parseInt(amount));
          console.log(answer);
          return reject(errrrr);
        }
        if (result.response.error !== '0') {
          // We couldn't process funds through eTranzact, return the user's funds
          const answer = await creditM(mobile, parseInt(amount));
          console.log(answer);
          return resolve({ success: false, message: 'Transaction Failed', data: result });
        }
        return resolve({ success: true, data: result });
      });
    });
  });
};

// receiver of the grpc call vtu
const vtu = async (call, callback) => {
  // Validate session
  const req = call.request;
  // eslint-disable-next-line
  processVTU(req.telco, req.amount, req.mobile)
    .then((response) => {
      // console.log(response);
      if (response.success) {
        addTransaction('vtu', req.eyowo_user, req.amount);
        return callback(null, {
          success: true,
          message: 'VTU purchase successful',
          data: JSON.stringify(response.data),
        });
      }
      response.data = JSON.stringify(response.data);
      return callback(null, {
        success: false,
        message: 'VTU purchase failed',
        data: JSON.stringify(response),
      });
    })
    .catch((errr) => {
      console.log(err);
      callback(null, {
        success: false,
        message: 'End Funds Transfer Failed',
        data: JSON.stringify(errr),
      });
    });
};

// receiver of the grpc call dstv
const dstv = async (call, callback) => {
  // Validate session
  const req = call.request;
  try {
    // eslint-disable-next-line
    const response = await processDSTV(req.mobile, req.amount, req.decoder);

    // console.log(response)
    if (response.success) {
      // eslint-disable-next-line
      addTransaction('dstv', req.mobile, req.amount);
      return callback(null, {
        success: true,
        message: 'DSTV purchase Successful',
        data: JSON.stringify(response.data),
      });
    }
    response.data = JSON.stringify(response.data);
    return callback(null, {
      success: false,
      message: 'Error in DSTV purchase',
      data: JSON.stringify(response),
    });
  } catch (err) {
    console.log(err);
    return callback(null, { success: false, message: 'Error', data: JSON.stringify(err) });
  }
};


module.exports = {
  vtu,
  dstv,
};
