const grpc = require('grpc');

const PROTO_PATH = `${__dirname}/protos/bills.proto`;
const fundsTransferProto = grpc.load(PROTO_PATH).fundstransfer;

const client = new fundsTransferProto.Bills(
  '0.0.0.0:50051',
  grpc.credentials.createInsecure(),
);

// client.dstv({
//   amount: 100, decoder: 'stdcvdvd12', eyowo_user: '10', mobile: '2348141804018',
// }, (err, response) => {
//   console.log(response);
//   console.log(err);
// });

client.vtu({
  eyowo_user: '10', telco: 'MTN', amount: 1000, mobile: '2348141804018',
}, (err, response) => {
  console.log(response);
  console.log(err);
});

