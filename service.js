const billctrl = require('./app/controllers/billsCtrl');
const grpc = require('grpc');

const PROTO_PATH = `${__dirname}/app/rpc/protos/bills.proto`;
const fundsTransferProto = grpc.load(PROTO_PATH).fundstransfer;

const server = new grpc.Server();
server.addService(fundsTransferProto.Bills.service, billctrl);
server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
server.start();
console.log('Server running @ localhost:50051');

